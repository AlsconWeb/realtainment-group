<?php
/**
 * Functions from TR Mod Child Theme.
 *
 * @package TR_MOD
 */


use TR_MOD\ThemeInit;

require_once __DIR__ . '/vendor/autoload.php';

new ThemeInit();
