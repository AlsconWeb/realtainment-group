<?php
/**
 * Theme Init class file.
 *
 * @package TR_MOD
 */

namespace TR_MOD;

/**
 * ThemeInit class file.
 */
class ThemeInit {
	/**
	 * Version theme.
	 */
	public const TR_VERSION = '1.0.1';


	/**
	 * ThemeInit construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Theme init function.
	 *
	 * @return void
	 */
	public function init(): void {
		add_action( 'wp_enqueue_scripts', [ $this, 'add_script' ] );
		add_action( 'ninja_forms_after_submission', [ $this, 'send_mail' ] );
	}

	/**
	 * Add Script and Style.
	 *
	 * @return void
	 */
	public function add_script(): void {
		wp_enqueue_script( 'tr_main', get_stylesheet_directory_uri() . '/assets/js/main.js', [ 'jquery' ], self::TR_VERSION, true );

		wp_enqueue_style( 'tr_main', get_stylesheet_directory_uri() . '/assets/css/main.css', [], self::TR_VERSION );
		wp_enqueue_style( 'tr_style', get_stylesheet_directory_uri() . '/style.css', [], self::TR_VERSION );
	}

	/**
	 * Send mail and send to api.
	 *
	 * @param array $args Arguments.
	 *
	 * @return void
	 */
	public function send_mail( $args ): void {
		$fields_array = [];
		foreach ( $args['fields'] as $key => $item ) {
			$fields_array[ $item['key'] ] = $item['value'];
		}

		$url = 'https://boards-api.greenhouse.io/v1/boards/realtainmentgmbh/jobs/4021267101';

		// phpcs:disable
		$api_key = base64_encode( '952d57d1d5f2b60b1379f9fce7ca1114-101' );
		// phpcs:enable

		$body = [
			'first_name'          => $fields_array['voename'],
			'last_name'           => $fields_array['nachname'],
			'email'               => $fields_array['e-mail'],
			'phone'               => $fields_array['phone'],
			'question_4093786101' => $fields_array['in_city'],
			'question_4093788101' => $fields_array['storage_space'],
			'question_4093789101' => $fields_array['acrylic_paints'],
			'question_4147789101' => $fields_array['find_out_about_job'],
			'question_4093787101' => $fields_array['can_start'],
			'question_4093790101' => $fields_array['referrens_name'] . ' ' . $fields_array['referens_email'],
			'data_compliance'     => [
				'gdpr_consent_given' => $fields_array['gdpr'] ?? false,
			],
		];

		$arguments = [
			'timeout'     => 45,
			'redirection' => 5,
			'httpversion' => '1.0',
			'headers'     => [
				'Authorization' => $api_key,
				'Content-Type'  => 'application/json',

			],
			'body'        => wp_json_encode( $body ),
		];

		$response = wp_remote_post( $url, $arguments );
	}
}

